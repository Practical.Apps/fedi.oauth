# FEDIVERSE OAUTH INTEGRATION GUIDE


**Summary**\
This document is a brief guide on how to incorporate OAuth authentication via a fediverse server for log-in on another website.
Two methods are discussed: the first being compatible with Pleroma and Mastodon software, the second specifically for Misskey.


**Why**\
While the protocols for interaction with fediverse software are well documented, integrating it into other websites is not.
I had to take a while reading docs to figure out how to do something which, as this guide should show you, is fairly trivial.


**Brief Theory**\
Before getting into the how-to of this guide, I think it is a good idea to explain what is happening first.
OAuth is a system for generating access tokens so that you can securely interface with a fediverse account programatically.
With this system you can access virtually all features of the account; this is how third party mobile applications can simulate
all the functionality of the regular website frontend. It is possible to adapt these instructions so that these other functions
are available to you and your program but, for the purposes of this guide, I will only be covering basic account authentication.
The idea being that OAuth can subsitute for the standard log-in system for your website/program (with the ability to do more).



## Pleroma/Mastodon

#### Creating an Application

The first thing that needs to be done to start interacting with a fediverse server is to create an application.

The application is a set of instructions that you are giving to the server you want to connect to.
With these instructions you are registering your program and its intent with the server.
You will be declaring a name for your program and a website for reference, a redirect link to send the user to,
along with the scope of the permissions which will be granted when a user generates an authentication token with your application.

The following sample comes from [the mastodon docs](https://docs.joinmastodon.org/client/token/):

```shell
curl -X POST \
-F 'client_name=Test Application' \
-F 'redirect_uris=urn:ietf:wg:oauth:2.0:oob' \
-F 'scopes=read write follow push' \
-F 'website=https://myapp.example' \
https://mastodon.example/api/v1/apps
```

This shows you the standard form of the packet which gets posted to the /api/v1/apps endpoint of the server.
The client name and website fields are really inconsequential, you can put whatever you want there.
What really matters are the scopes and redirect link(s) you define here.

The possible scopes are listed [here](https://docs.joinmastodon.org/api/oauth-scopes/), for this example we will be using read:accounts.
This is probably the lowest level of permissions you can have, ":accounts" indicates that you do not have full "read" permissions, only account data.
The redirect_uris field defines either one link where a user will be sent after entering their credentials, or a list of links to which they could
possibly be sent. In my example this is a single login redirect link which returns the necessary data to my webserver. It's worth mentioning that 
the redirect link can be a local address if you're running a local development server, using http://localhost:8000 here is perfectly valid.

The following is what the typical fedifinder application packet looks like:

```shell
curl -X POST \
-F 'client_name=FediFinder_3LA' \
-F 'redirect_uris=https://www.fedifinder.com/find/login/redirect/instance.tld' \
-F 'scopes=read:accounts' \
-F 'website=https://www.fedifinder.com' \
https://instance.tld/api/v1/apps
```

When you send this data to the server, the response you get back looks like this:

*Pleroma*

{"client_id":"QWERTYUIOP12345","client_secret":"ASDFGHJKL6789","id":"1234567","name":"FediFinder_3LA","redirect_uri":"https://www.fedifinder.com/find/login/redirect/instance.tld", "website":"https://www.fedifinder.com", "vapid_key":"ZXCVBNM0123456789"}

*Mastodon*

{"id":"123","name":"FediFinder_3LA","website":"https://www.fedifinder.com", "redirect_uri":"https://www.fedifinder.com/find/login/redirect/instance.tld", "client_id":"QWERTYUIOP12345","client_secret":"ASDFGHJKL6789","vapid_key":"ZXCVBNM0123456789"}

The important pieces of data here are the client ID and client secret, you will use them to create the requests to the server for each user.
It's implicit in the word 'secret' but in case you are unfamiliar with public-private key-pairs, the client secret should be KEPT SECRET.
You'll want to save this information to a file somewhere for access but try to avoid letting that become publicly viewable.

#### Getting the Authorization Code

The next step in the process is to use the application information to direct the user to the authentication page.
To do this you create a URL for the server starting with /oauth/authorize and appending the data from your application.

This is what the link would look like given the sample/placeholder data I've been using for this example:

https://instance.tld/oauth/authorize?client_id=QWERTYUIOP12345&scope=read:accounts&redirect_uri=https://fedifinder.com/find/login/redirect/instance.tld&response_type=code

Make sure the data you include in your URL matches the data in the application. The OAuth page will throw an error if anything doesn't match.
                    
When users are directed to this link they will see the typical login form from their instance. After they input valid credentials, they will
be redirected to the link you provide in the URL above, and an authorization code will be appended to the end of the URL. In my case,
they will return to the site with this link in their URL:

https://fedifinder.com/find/login/redirect/instance.tld?code=QAZWSXEDCRFVTGBYHNUJMIKOLP

#### Getting the Authorization Token

With this code we create a new data packet to send to the server. The form is defined [here](https://docs.joinmastodon.org/methods/apps/oauth/#obtain-a-token)
in the Mastodon docs.

```shell
curl -X POST \
-F 'client_id=QWERTYUIOP12345' \
-F 'client_secret=ASDFGHJKL6789' \
-F 'grant_type=authorization_code' \
-F 'code=QAZWSXEDCRFVTGBYHNUJMIKOLP' \
-F 'redirect_uri=https://fedifinder.com/find/login/redirect/instance.tld' \
-F 'scope=read:accounts' \
https://instance.tld/oauth/token
```

There is not really any nuance here, you just have to copy the code we just received in the previous step into the code field of the packet.
Where it says "authorization_code" is not a placeholder value, this is an instruction to the server for the type of response.

The expected response will look something like this:

{"access_token": "PLOKMIJNUHBYGVTFCRDXESZWAQ","token_type": "Bearer","scope": "read:accounts","created_at": 1601234567}

Here the access token is the only important piece of information, it will be used in the next data packet.

#### Verifying Credentials

The final step in the process is using the token we just created to retreive the user data. The last packet looks like this:

```shell
curl -X POST \
-H 'Authorization: Bearer PLOKMIJNUHBYGVTFCRDXESZWAQ' \
https://instance.tld/api/v1/accounts/verify_credentials
```

A key difference to note is that this packet only includes the header, there is no data body to the message.

The response should include all data points for the account, I will not include a sample result here but you can see one in
[the docs](https://docs.joinmastodon.org/methods/accounts/) under "Verify Credentials."

The fields I'm interested in for fedifinder are:

'acct' - the username \
'display_name' - self-explanitory \
'avatar' - a link to the user's avatar image \
'header' - a link to the user's header image \
'created_at' - the time that the user's account was created \
'note' - the user's "bio" field \
'url' - a link to the user's profile page

The username, URL, and creation time fields are good choices for data to check against as they will not change.
In my case I combine the username with the instance domain to create a full fediverse tag (e.g. abc@xyz.tld) 
and then on the backend I can use standard code that treats the tag just like an e-mail address.



## Misskey

Misskey is a different breed of fediverse software. Everything is still compatible but just done a bit differently.
Comparing this with the Pleroma/Mastodon method, you should notice it is a bit shorter and simpler.

#### Creating an Application

The first step here is the same, we register our application with the server. You'll notice some slight variations here though:

```shell
curl -X POST \
-H 'Content-Type: application/json' \
-d '{"name": "FediFinder_3LA","description":"https://www.fedifinder.com", "permission": ["read:account"],"callbackUrl":"https://www.fedifinder.com/find/login/redirect/instance.tld"}' \
https://instance.tld/api/app/create
```

Most important is that the data packet to be sent has to be JSON encoded rather than URL encoded like our previous example.
Another important difference is the scope/permission is "read:account" singular, no 's' at the end. The list of permissions in Misskey
can be found in their repo in the file [api-permissions.ts](https://github.com/misskey-dev/misskey/blob/develop/packages/backend/src/misc/api-permissions.ts).
You will notice other minor differences such as "callbackUrl" instead of "redirect_uri" but these discrepancies are not important
when you understand they are the exact same thing just with a new name.

The data which is returned from this should look something like:

{"id":"8w8QWERTY","name":"FediFinder_3LA","callbackUrl":"https://www.fedifinder.com/find/login/redirect/instance.tld", "permission":["read:account"],"secret":"ASDFGHJKL6789"}

The only important data point here is the "secret" as the "id" field does not get used in the same way as with Pleroma/Mastodon.

#### Getting the Authorization Code

The next step in the process is also the same, using the application information to direct the user to the authentication page.
The difference here is how the OAuth URLs are created. Rather than an ad-lib-able URL template in Pleroma/Mastodon, with Misskey
the server returns a unique URL to you when you post a packet in this form:

```shell
curl -X POST \
-H 'Content-Type: application/json' \
-d '{"appSecret":"ASDFGHJKL6789"}' \
https://instance.tld/api/auth/session/generate
```

As far as I know there is not a central Misskey doc website, instead the docs are included on each Misskey instance. You can view
them by appending "/api-doc" to the end of the instance URL. For this section specifically, reference https://instance.tld/api-doc#tag/auth.
The data that gets returned should look like this:

{"token":"qwerty123-4567-8910-abcd-zxcvbnm","url":"https://instance.tld/auth/qwerty123-4567-8910-abcd-zxcvbnm"}

This "url" is the counterpart to the OAuth URL from the previous example. They go to essentially the same page, a user/password login screen.
The result of the user logging in will be the same, they are redirected to the provided "callbackUrl" with one important change.
Instead of "?code=" at the end of the URL, the variable given is named "token" and it is the same as the token from the step above.
Rather than returning a new code the token is not active until the user completes their authentication on their instance. If you try to
send the next packet before this has been finished you will get an error saying that "this session is not completed yet."

#### Verifying Credentials

The final step in the process is also the same, using the token we just received to get the user data. The last data packet looks like this:

```shell
curl -X POST \
-H 'Content-type: application/json' \
-d '{"appSecret":"ASDFGHJKL6789","token":"qwerty123-4567-8910-abcd-zxcvbnm"}' \
https://instance.tld/api/auth/session/userkey
```

The data that gets returned from /auth/session/userkey is virtually the same as what comes from /accounts/verify_credentials on Pleroma/Mastodon.
Again, I will not include a sample user data object here, but you can see one at /api-doc#operation/auth/session/userkey on your Misskey instance.
The user data is practically identical just with different variable names. Using the fields mentioned before as examples:

'acct' is instead called 'username' \
'display_name' is called 'name' \
'avatar' is called 'avatarUrl' \
'header' is called 'bannerUrl' \
'created_at' is restyled as 'createdAt' \
'note' is called 'description'

And that's all there is to it. With this information you should have a much easier time integrating fediverse accounts into your project.